led.set_brightness(45)
basic.show_string("compassTx")
basic.pause(2000)
radio.set_group(6)

def on_every_interval():
    radio.send_number(input.compass_heading())
loops.every_interval(20, on_every_interval)

def on_every_interval2():
    basic.show_leds("""
        . . . . .
                . . . . .
                . . # . .
                . . . . .
                . . . . .
    """)
    basic.pause(5)
    basic.clear_screen()
loops.every_interval(1000, on_every_interval2)
