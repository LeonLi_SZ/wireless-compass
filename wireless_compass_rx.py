def on_received_number(receivedNumber):
    global dataProcessed, inDeg, outDeg, outRad
    if dataProcessed:
        dataProcessed = False
        inDeg = receivedNumber
        if inDeg <= 90:
            outDeg = Math.map(inDeg, 0, 90, 90, 0)
        else:
            outDeg = Math.map(inDeg, 91, 359, 359, 91)
        outDeg = Math.constrain(outDeg, 0, 359)
        # rad = deg / 180  * 3.1415926  = deg * 3.1415926 / 180 = deg * 0.017453292
        outRad = outDeg * 0.017453292
        control.raise_event(4800, 4801)
radio.on_received_number(on_received_number)

def on_on_event():
    global outDegNoTan, tanOutRad, toUARTprint, dataProcessed
    if outDeg == 90 or outDeg == 270:
        outDegNoTan = True
    else:
        outDegNoTan = False
        tanOutRad = Math.tan(outRad)
    sectorProcess()
    mapMathXY_oledXY()
    drawAngleLine()
    if toUARTprint:
        toUARTprint = False
        uartPrint()
    dataProcessed = True
control.on_event(4800, 4801, on_on_event)

def sectorProcess():
    global sector, mathX, mathY
    if outDeg <= 27:
        sector = 4
        mathX = 63
        mathY = int(63 * tanOutRad)
    elif outDeg <= 153:
        sector = 1
        mathY = 31
        if outDegNoTan:
            mathX = 0
        else:
            mathX = int(31 / tanOutRad)
    elif outDeg <= 207:
        sector = 2
        mathX = -62
        mathY = int(-62 * tanOutRad)
    elif outDeg <= 333:
        sector = 3
        mathY = -30
        if outDegNoTan:
            mathX = 0
        else:
            mathX = int(-30 / tanOutRad)
    else:
        sector = 4
        mathX = 63
        mathY = int(63 * tanOutRad)
def drawAngleLine():
    OLED.clear()
    OLED.draw_line(63, 31, oledX, oledY)
def mapMathXY_oledXY():
    global oledX, oledY
    oledX = Math.constrain(pins.map(mathX, -63, 64, 1, 126), 1, 126)
    oledY = Math.constrain(pins.map(mathY, 32, -31, 1, 62), 1, 62)
def uartPrint():
    soroban.show_number(sector * 10000 + inDeg, Align.RIGHT, True)
    serial.write_value("inDeg", inDeg)
    serial.write_string("")
    serial.write_value("outDeg", outDeg)
    serial.write_string("")
    serial.write_string("")
    serial.write_value("outRad", outRad)
    serial.write_string("")
    serial.write_value("tan(k)", tanOutRad)
    serial.write_string("")
    serial.write_value("mathX", mathX)
    serial.write_string("")
    serial.write_value("mathY", mathY)
    serial.write_string("")
    serial.write_value("oledX", oledX)
    serial.write_string("")
    serial.write_value("oledY", oledY)
    serial.write_string("")
    serial.write_value("sector", sector)
    serial.write_string("==========")
    serial.write_line("")
    serial.write_line("")
oledY = 0
oledX = 0
mathY = 0
mathX = 0
sector = 0
tanOutRad = 0
outDegNoTan = False
outRad = 0
outDeg = 0
inDeg = 0
toUARTprint = False
dataProcessed = False
OLED.init(128, 64)
OLED.write_string_new_line("compass Rx")
OLED.write_string_new_line("Freda Li")
OLED.write_string_new_line("2023 3 16")
basic.show_string("compassRx")
serial.write_line("wireless compass Rx")
basic.pause(2000)
dataProcessed = True
toUARTprint = False
radio.set_group(6)

def on_every_interval():
    global toUARTprint
    toUARTprint = True
loops.every_interval(3000, on_every_interval)

def on_every_interval2():
    led.set_brightness(Math.constrain(pins.map(input.light_level(), 0, 180, 50, 200), 50, 200))
loops.every_interval(3000, on_every_interval2)
